package com.example.currencyexchange;

public class NewCourseJSON {

    private String Date;
    private String PreviousDate;
    private String PreviousURL;
    private String Timestamp;
    private CourseValuteJSON[] courseValuteJSONS;

    private int Count;

    public String getDate(){
        return this.Date;
    }
    public void setDate(String Date){
        this.Date = Date;
    }

    public String getPreviousDate(){
        return PreviousDate;
    }
    public void setPreviousDate(String PreviousDate){
        this.PreviousDate = PreviousDate;
    }

    public String getPreviousURL(){
        return PreviousURL;
    }
    public void setPreviousURL(String PreviousURL){
        this.PreviousURL = PreviousURL;
    }

    public String getTimestamp(){
        return Timestamp;
    }
    public void setTimestamp(String Timestamp){
        this.Timestamp = Timestamp;
    }

    public CourseValuteJSON getCourseValuteJSON(int i) {
        return courseValuteJSONS[i];
    }

    public void setCourseValuteJSON(CourseValuteJSON[] valute) {
        this.courseValuteJSONS = valute;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n Date:" + this.Date);
        sb.append("\n PreviousDate:" + this.PreviousDate);
        sb.append("\n PreviousURL:" + this.PreviousURL);
        sb.append("\n courseValuteJSONS:" + this.courseValuteJSONS);
        if (this.courseValuteJSONS != null) {
            sb.append("\n address:" + this.courseValuteJSONS.toString());
        }
        return sb.toString();
    }
    public int getCount(){
        return this.Count;
    }
    public void setCount(int Count){
        this.Count = Count;
    }


}
