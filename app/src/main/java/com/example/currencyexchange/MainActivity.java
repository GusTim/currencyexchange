package com.example.currencyexchange;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Time;
import java.util.ArrayList;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;

public class MainActivity extends AppCompatActivity {

    ArrayList<ExchangeRate> exchangeRate = new ArrayList<ExchangeRate>();
    ExchangeRateAdapter exchangeRateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText valueRUBEditText = (EditText)findViewById(R.id.edtRUB);
        ImageButton exchangeButton =(ImageButton) findViewById(R.id.imbExchange);
        EditText valuteExchange = (EditText)findViewById(R.id.edtValue);
        TextView textView = (TextView) findViewById(R.id.textView);


        valuteExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SimpleSearchDialogCompat(MainActivity.this, "Search...",
                        "What are you looking for...?", null, initdata(),
                        new SearchResultListener<SearchModel>() {
                            @Override
                            public void onSelected(BaseSearchDialogCompat dialog,
                                                   SearchModel item, int position) {
                                valuteExchange.setText(item.getTitle());

                                Toast.makeText(MainActivity.this, item.getTitle(),
                                        Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
        exchangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueRUB = valueRUBEditText.getText().toString();

            }
        });
        runExample(1);
        exchangeRateAdapter = new ExchangeRateAdapter(this, exchangeRate);

        ListView lvCurrencies =(ListView) findViewById(R.id.listviewCurrencies);
        lvCurrencies.setAdapter(exchangeRateAdapter);


    }

    public void runExample(double k)  {
        try {

            NewCourseJSON newCourseJSON = CourseUpdate.readJSONFile(this);
            int count = newCourseJSON.getCount()-1;
            char[] TimeData = newCourseJSON.getDate().toCharArray();
            String Time = "";
            String Data = "";
            String TimeZone = "";
            for (int n = 11; n <= 18; n++){
                Time += TimeData[n];
            }
            for (int n = 0; n <= 9; n++){
                Data += TimeData[n];
            }
            for (int n = 19; n <= 24; n++){
                TimeZone += TimeData[n];
            }
            String ExchangeDate = "Time: "+Time+" Data:"+Data+" TimeZone:"+TimeZone;

            for (int i = 0; i <= count; i++){
                double RUB = k*Double.parseDouble(newCourseJSON.getCourseValuteJSON(i).getValue());
                String Exchange = "1 "+newCourseJSON.getCourseValuteJSON(i).getCharCode()+" = "
                        + RUB+" RUB";

                exchangeRate.add(new ExchangeRate(Exchange, ExchangeDate));
            }
        } catch(Exception e)  {
            for (int i = 1; i <=1 ; i++){
                exchangeRate.add(new ExchangeRate("No info", "No info"));
            }
            e.printStackTrace();
        }

    }
    private ArrayList<SearchModel> initdata(){
        ArrayList<SearchModel> items = new ArrayList<>();
        try {

            NewCourseJSON newCourseJSON = CourseUpdate.readJSONFile(this);
            int count = newCourseJSON.getCount()-1;

            for (int i = 0; i <= count; i++){
                String ValuteName = newCourseJSON.getCourseValuteJSON(i).getName();
                items.add(new SearchModel(ValuteName));
            }
        } catch(Exception e)  {

            e.printStackTrace();
        }
        return items;
    }

}