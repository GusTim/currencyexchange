package com.example.currencyexchange;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ExchangeRateAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater layoutInflater;
    ArrayList<ExchangeRate> object;

    ExchangeRateAdapter(Context context, ArrayList<ExchangeRate> exchangeRates){
        ctx = context;
        object = exchangeRates;
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return object.size();
    }

    @Override
    public Object getItem(int position) {
        return object.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null){
            view = layoutInflater.inflate(R.layout.item, parent, false);
        }
        ExchangeRate exchangeRate = getexchangeRate(position);

        ((TextView) view.findViewById(R.id.date)).setText(exchangeRate.data);
        ((TextView) view.findViewById(R.id.exchange_rate)).setText(exchangeRate.exchange);

        return view;
    }

    ExchangeRate getexchangeRate(int position){
        return ((ExchangeRate) getItem(position));
    }
}
