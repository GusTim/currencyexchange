package com.example.currencyexchange;

import android.content.Context;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CourseUpdate {

    public static NewCourseJSON readJSONFile(Context context) throws IOException, JSONException {

        String jsonText = readText(context, R.raw.value);
        JSONObject jsonRoot = new JSONObject(jsonText);

        String Date = jsonRoot.getString("Date");
        String PreviousDate = jsonRoot.getString("PreviousDate");
        String PreviousURL = jsonRoot.getString("PreviousURL");
        String Timestamp = jsonRoot.getString("Timestamp");
        NewCourseJSON newJSONCourse = new NewCourseJSON();
        try {
            JSONObject object = new  JSONObject(jsonText);
            JSONObject Jobject  =  object.getJSONObject("Valute");
            newJSONCourse.setDate(Date);
            newJSONCourse.setPreviousDate(PreviousDate);
            newJSONCourse.setPreviousURL(PreviousURL);
            newJSONCourse.setTimestamp(Timestamp);

            JSONArray arr = Jobject.names();
            String[] ValuteName = new String[arr.length()];
            for(int i=0;i < arr.length();i++) {
                ValuteName[i] = arr.getString(i);

            }

            CourseValuteJSON[] valute = new CourseValuteJSON[Jobject.length()];
            for (int i = 0; i < Jobject.length(); i++) {

                JSONObject jsonValute = (JSONObject) Jobject.get(ValuteName[i]);

                String ID = jsonValute.getString("ID");
                String NumCode= jsonValute.getString("NumCode");
                String CharCode= jsonValute.getString("CharCode");
                String Nominal= jsonValute.getString("Nominal");
                String Name = jsonValute.getString("Name");
                String Value = jsonValute.getString("Value");
                String Previous= jsonValute.getString("Previous");

                valute[i] = new CourseValuteJSON(ID,NumCode,CharCode,Nominal,Name,Value,Previous);
                newJSONCourse.setCourseValuteJSON(valute);
            }
            newJSONCourse.setCount(Jobject.length());
            return newJSONCourse;
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("TAG", "Error parsing data " + e.toString());
        }
        newJSONCourse.setDate(Date);
        newJSONCourse.setPreviousDate(PreviousDate);
        return newJSONCourse;
    }

    private static String readText(Context context, int resId) throws IOException {
        InputStream is = context.getResources().openRawResource(resId);
        BufferedReader br= new BufferedReader(new InputStreamReader(is));
        StringBuilder sb= new StringBuilder();
        String s= null;
        while((  s = br.readLine())!=null) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}
