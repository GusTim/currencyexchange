package com.example.currencyexchange;

public class CourseValuteJSON {
    private String ID;
    private String NumCode;
    private String CharCode;
    private String Nominal;
    private String Name;
    private String Value;
    private String Previous;

    public CourseValuteJSON(String _ID, String _NumCode, String _CharCode, String _Nominal, String _Name,
                            String _Value, String _Previous) {
        this.ID = _ID;
        this.NumCode = _NumCode;
        this.CharCode = _CharCode;
        this.Nominal = _Nominal;
        this.Name = _Name;
        this.Value = _Value;
        this.Previous = _Previous;
    }

    public String getID(){
        return this.ID;
    }
    public void setID(String ID){
        this.ID = ID;
    }

    public String getNumCode(){
        return NumCode;
    }
    public void setNumCode(String NumCode){
        this.NumCode = NumCode;
    }

    public String getCharCode(){
        return CharCode;
    }
    public void setCharCode(String CharCode){
        this.CharCode = CharCode;
    }

    public String getNominal(){
        return Nominal;
    }
    public void setNominal(String Nominal){
        this.Nominal = Nominal;
    }

    public String getName(){
        return Name;
    }
    public void setName(String Name){
        this.Name = Name;
    }
    public String getValue(){
        return Value;
    }
    public void setValue(String Value){
        this.Value = Value;
    }

    public String getPrevious(){
        return Previous;
    }
    public void setPrevious(String Previous){
        this.Previous = Previous;
    }



    @Override
    public String toString() {
        return ID + ", " + NumCode+", " +CharCode+ ", "+Nominal + ", "+ Name+ ", " + Value+ ", "+Previous;
    }
}
